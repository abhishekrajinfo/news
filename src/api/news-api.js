export function fetchNews(searchText = "", pageNumber = 1) {
  return fetch(
    `https://content.guardianapis.com/search?api-key=test&q=${searchText}&show-fields=thumbnail,headline&show-tags=keyword&page=${pageNumber}&page-size=10`
  );
}
