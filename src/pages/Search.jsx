import React, { useState } from "react";
import { useRef } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import List from "../components/List";
import Pagination from "../components/Pagination";
import { getNews } from "../store/newsSlice";
import styles from "./Search.module.css";

export default function Search() {
  const [searchText, setSearchText] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [hasError, setHasError] = useState(false);
  const { results, pages, pageSize, total } = useSelector(
    (state) => state.news.data
  );
  const dispatch = useDispatch();

  const inputRef = useRef();

  useEffect(() => {
    inputRef.current.focus();
  }, []);

  useEffect(() => {
    if (searchText) {
      dispatch(getNews({ searchText, pageNumber: currentPage }));
    }
  }, [currentPage, dispatch, searchText]);

  const onSubmit = (e) => {
    e.preventDefault();
    if (searchText === "") {
      setHasError(true);
    } else {
      setHasError(false);
      dispatch(getNews({ searchText, pageNumber: currentPage }));
    }
  };

  return (
    <div>
      <h1 className={styles.alignCenter}>News Lister</h1>
      <div className={styles.alignCenter}>
        <form onSubmit={onSubmit}>
          <input
            className={styles.input}
            value={searchText}
            ref={inputRef}
            onChange={(e) => setSearchText(e.target.value)}
            placeholder="Enter a keyword"
          />
          <button className={styles.button} onClick={onSubmit}>
            Search
          </button>
        </form>
        {hasError && (
          <p className={styles.errorText}>You must enter a keyword</p>
        )}
      </div>
      {results?.length ? (
        <>
          <List setCurrentPage={setCurrentPage} setSearchText={setSearchText} />
          <Pagination
            total={total}
            pages={pages}
            pageSize={pageSize}
            setCurrentPage={setCurrentPage}
            currentPage={currentPage}
          />
        </>
      ) : (
        <></>
      )}
    </div>
  );
}
