import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { fetchNews } from "../api/news-api";

const initialState = {
  status: "idle",
  data: {
    results: [],
  },
};

export const getNews = createAsyncThunk(
  "news/fetch",
  async ({ searchText, pageNumber }) => {
    const response = await fetchNews(searchText, pageNumber);
    const data = await response.json();
    return data.response;
  }
);

export const newsSlice = createSlice({
  name: "news",
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(getNews.pending, (state) => {
        state.status = "loading";
      })
      .addCase(getNews.fulfilled, (state, action) => {
        state.status = "idle";
        state.data = action.payload;
      });
  },
});

export default newsSlice.reducer;
