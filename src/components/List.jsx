import React from "react";
import { useSelector } from "react-redux";

import styles from "./List.module.css";
import Card from "./Card";

export default function List({ setCurrentPage, setSearchText }) {
  const { results } = useSelector((state) => state.news.data);

  return (
    <div className={styles.list}>
      {results.length ? (
        results.map((news) => {
          return (
            <Card
              setCurrentPage={setCurrentPage}
              setSearchText={setSearchText}
              key={news.id}
              news={news}
            />
          );
        })
      ) : (
        <></>
      )}
    </div>
  );
}
