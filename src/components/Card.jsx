import React from "react";
import styles from "./Card.module.css";
import defaultImage from "../images/defaultImage.png";
import { useDispatch } from "react-redux";
import { getNews } from "../store/newsSlice";
import { scrollTop } from "../utils";

export default function Card({
  news: {
    fields: { headline, thumbnail = defaultImage },
    webUrl,
    tags,
  },
  setCurrentPage,
  setSearchText,
}) {
  const dispatch = useDispatch();
  const openURL = () => {
    window.open(webUrl, "_blank");
  };

  const onTagClick = (text) => {
    setSearchText(text);
    setCurrentPage(1);
    dispatch(getNews({ searchText: text }));
    scrollTop();
  };

  return (
    <div className={styles.card}>
      <figure className={styles.cardImage}>
        <img
          className={styles.image}
          src={thumbnail}
          alt={headline}
          onClick={openURL}
        />
      </figure>

      <div className={styles.cardContent}>
        <h2 className={styles.title} onClick={openURL}>
          {headline}
        </h2>

        {tags.length ? (
          tags.map((tag) => {
            return (
              <span
                key={tag.webUrl}
                className={styles.tag}
                onClick={() => onTagClick(tag.webTitle)}
              >
                {tag.webTitle}
              </span>
            );
          })
        ) : (
          <></>
        )}
      </div>
    </div>
  );
}
