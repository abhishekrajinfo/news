import React from "react";
import { scrollTop } from "../utils";
import styles from "./Pagination.module.css";

export default function Pagination({
  pageSize,
  pages,
  currentPage,
  setCurrentPage,
  total,
}) {
  const onPageChange = (event) => {
    const pageNumber = Number(event.target.textContent);
    setCurrentPage(pageNumber);
    scrollTop();
  };

  const onNextClick = () => {
    setCurrentPage((page) => page + 1);
    scrollTop();
  };

  const onPrevClick = () => {
    setCurrentPage((page) => page - 1);
    scrollTop();
  };

  const paginationNumbers = () => {
    let start = Math.floor((currentPage - 1) / pageSize) * pageSize;
    const numbersToGenerate = pages > pageSize ? pageSize : pages;
    const pageNumbers = new Array(numbersToGenerate)
      .fill()
      .map((_, idx) => start + idx + 1);
    return pageNumbers;
  };

  return (
    <div className={styles.pagination}>
      <button
        onClick={onPrevClick}
        className={`${styles.prev} ${currentPage === 1 ? styles.disabled : ""}`}
      >
        prev
      </button>

      {paginationNumbers().map((item, index) => (
        <button
          key={index}
          onClick={onPageChange}
          className={`${styles.paginationItem} ${
            currentPage === item ? styles.active : null
          }`}
        >
          {item}
        </button>
      ))}

      <button
        onClick={onNextClick}
        className={`${styles.next} ${
          currentPage === pages ? styles.disabled : ""
        }`}
      >
        next
      </button>
    </div>
  );
}
